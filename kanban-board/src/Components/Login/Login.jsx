import React, { useEffect, } from 'react';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './Login.scss';


const Login = ({ setIsValid, data, email, setEmail, password, setPassword }) => {

    const navigate = useNavigate()
    const HandleSubmit = () => {
        data.filter((item) => {
            if (email === item.userEmail && password === item.userPassword) {
                setIsValid(true)
                navigate('/Dashboard');
            } else {
                alert("Please enetr correct details")
            }
        })
    }
    const handleNewUser = () => {
        navigate('/signup')
    }
    return (


        <div className='login'>
            <h2 className='LoginHeading'>Login</h2>
            <form style={{ width: "20rem" }}>
                <input type="text" placeholder='Email' onChange={(e) => setEmail(e.target.value)} />
                <input type="password" placeholder='Password' onChange={(e) => setPassword(e.target.value)} />

                <button
                    className="button"
                    type="submit"
                    onClick={HandleSubmit}
                >
                    Login
                </button><br></br>
                <a onClick={handleNewUser}>Don't Have Account?     <a className="Register" >Register here</a>
                </a>
            </form>

        </div>

    );
};

export default Login;