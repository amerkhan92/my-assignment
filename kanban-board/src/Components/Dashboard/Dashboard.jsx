import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import StatusLine2 from '../StatusLine2';

const Dashboard = ({ data, email, password }) => {
    const [loggedInUserName, setloggedInUserName] = useState("")
    useEffect(() => {
        data.filter((item) => {
            if (email === item.userEmail && password === item.userPassword) {
                setloggedInUserName(item.userName)
            }
        })
    })
    return (
        <div>
            <h5>{loggedInUserName}</h5>
            <StatusLine2 />
        </div>
    );
};

export default Dashboard;
