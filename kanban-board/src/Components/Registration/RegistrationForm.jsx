import React, { useEffect, useState } from "react";
import axios from "axios"
import { useNavigate } from 'react-router-dom';
import './Registration.css'
export const RegistrationForm = () => {
  const [userName, setUserName] = useState("")
  const [userEmail, setUserEmail] = useState("")
  const [userContact, setUserContact] = useState("")
  const [userPassword, setUserPassword] = useState("")

  const navigate = useNavigate()
  const postUserDetails = async () => {
    if (userEmail && userPassword && userName && userContact) {
      await axios.post("http://localhost:3000/UsersDetails",
        {
          userEmail: userEmail,
          userPassword: userPassword,
          userName: userName,
          userContact: userContact
        })
      navigate("/")
    }
  }

  const handleChange = (e) => {
    // const { UserName, email, contact, password } = e.target
    // console.log("e.target--->", userName)
  }
  return (


    <div className="register">
      <h3>sign up</h3>

      <label htmlFor="">User Name</label><br />
      <input type="text" name="name"
        autoComplete="off"
        onChange={(e) => setUserName(e.target.value)}

      />
      <br />
      <label htmlFor="">User Email</label><br />

      <input type="email" name="email"
        autoComplete="off"
        onChange={(e) => setUserEmail(e.target.value)} /> <br />

      <label htmlFor="">User Contact</label> <br />
      <input type="text" name="contact"
        autoComplete="off"
        onChange={(e) => setUserContact(e.target.value)}
      />

      <label htmlFor="">Password</label> <br />
      <input type="password" name="password"
        autoComplete="off"
        onChange={(e) => setUserPassword(e.target.value)}
      /> <br />
      <button
        className="btn btn-dark mt-3"
        type="submit"
        onClick={postUserDetails}
        style={{
        }}
      >
        Submit
      </button>

    </div>

  );
};
