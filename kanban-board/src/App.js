import './App.css';
// import "./styles/App.scss";

import { RegistrationForm } from './Components/Registration/RegistrationForm';
import { Routes, Route } from 'react-router-dom';
import Dashboard from './Components/Dashboard/Dashboard';
import Login from './Components/Login/Login';
import { useState, useEffect } from 'react';
import axios from "axios"


function App() {
  const [isValid, setIsValid] = useState(false)
  const [data, setData] = useState([])
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const getUserDetails = async () => {
    const result = await axios.get("http://localhost:3000/UsersDetails")
    setData([...result.data])

  }
  useEffect(() => {
    getUserDetails()
  }, [])

  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Login setIsValid={setIsValid} data={data} email={email} setEmail={setEmail} password={password} setPassword={setPassword} />} />
        {isValid &&
          <Route path="/Dashboard" element={<Dashboard setIsValid={setIsValid} data={data}
            setData={setData} email={email} password={password} />} />
        }
        <Route path="/signup" element={<RegistrationForm />} />
      </Routes>
    </div>
  );
}

export default App;
